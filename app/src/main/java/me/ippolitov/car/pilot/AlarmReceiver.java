package me.ippolitov.car.pilot;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import me.ippolitov.car.lib.Packet;

public class AlarmReceiver extends WakefulBroadcastReceiver {
    public static final String KEEP_ALIVE_INTENT = "me.ippolitov.car.pilot.KEEP_ALIVE";

    @Override
    public void onReceive(Context context, Intent intent) {
        Packet packet = Packet.builder().build();

        CarPilotApplication applicationContext = (CarPilotApplication) context.getApplicationContext();
        applicationContext.getDataMediator().sendFromUDP(packet);
    }
}
