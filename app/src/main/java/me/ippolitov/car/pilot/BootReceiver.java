package me.ippolitov.car.pilot;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BootReceiver extends WakefulBroadcastReceiver {
    private Logger LOG = LoggerFactory.getLogger(BootReceiver.class);

    @Override
    public void onReceive(Context context, Intent intent) {
        LOG.info("Boot event received");
        //  Nothing to do here: all work is done in CarPilotApplication
    }
}