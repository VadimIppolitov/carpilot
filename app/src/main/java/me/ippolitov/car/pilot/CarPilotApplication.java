package me.ippolitov.car.pilot;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.*;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import me.ippolitov.car.lib.CarUDPService;
import me.ippolitov.car.lib.DataMediator;
import me.ippolitov.car.lib.Packet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CarPilotApplication extends Application {
    private Logger LOG = LoggerFactory.getLogger(CarPilotApplication.class);

    private static final int KEEP_ALIVE_SECONDS = 30;

    private final BroadcastReceiver btBroadcastReceiver = new BtStateReceiver();
    private final AlarmReceiver alarmReceiver = new AlarmReceiver();

    private DataMediator dataMediator = new DataMediator();
    private ServiceConnection btServiceConnection;
    private CarPilotBTService btService;

    @Override
    public void onCreate() {
        super.onCreate();
        LOG.info("Application is starting...");

        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        getApplicationContext().registerReceiver(btBroadcastReceiver, filter);

        Intent udpIntent = new Intent(getApplicationContext(), CarUDPService.class);
        this.startService(udpIntent);
        ServiceConnection udpServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder binder) {
                LOG.info("UDP service connected!");
                CarUDPService udpService = ((CarUDPService.UDPBinder) binder).getService();
                udpService.setRebootHackEnabled(true);
                dataMediator.setUdpService(udpService);
                udpService.setDataMediator(dataMediator);
                initForeground(udpService);
                dataMediator.sendViaUDP(Packet.builder().putMagic().putKeepaliveMarker().build());
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                LOG.info("UDP service disconnected!");
            }
        };
        this.bindService(udpIntent, udpServiceConnection, 0);

        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        if (btAdapter == null) {
            LOG.error("Device does not support Bluetooth!");
        } else {
            if (btAdapter.isEnabled()) {
                LOG.debug("Bluetooth was already enabled, starting service");
                startBTServiceIfNotStarted();
            } else {
                LOG.debug("Bluetooth was disabled, trying to enable");
                btAdapter.enable();
            }
        }
    }

    private void initForeground(CarUDPService udpService) {
        Intent notificationIntent = new Intent(udpService, CarPilotActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(udpService, 0, notificationIntent, 0);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(udpService)
                .setSmallIcon(R.drawable.ic_star)
                .setTicker("Car service is running")
                .setContentTitle("Car service")
                .setContentText("Car service is running")
                .setContentIntent(pendingIntent);
        udpService.startForeground(1, notificationBuilder.build());
    }

    private void startBTServiceIfNotStarted() {
        if (btServiceConnection != null) {
            LOG.info("BT service already running, not starting again");
        }
        Intent btIntent = new Intent(this, CarPilotBTService.class);
        this.startService(btIntent);
        btServiceConnection = new ServiceConnection() {
            PendingIntent recurringIntent;

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                LOG.info("BT service connected!");
                btService = ((CarPilotBTService.BTBinder) service).getService();
                btService.setDataMediator(dataMediator);
                dataMediator.addOtherEnd(btService);

                getApplicationContext().registerReceiver(alarmReceiver, new IntentFilter(AlarmReceiver.KEEP_ALIVE_INTENT));

                Intent intent = new Intent(AlarmReceiver.KEEP_ALIVE_INTENT);
                recurringIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                final int keepAliveMillis = KEEP_ALIVE_SECONDS * 1000;
                getAlarmManager().setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, keepAliveMillis, keepAliveMillis, recurringIntent);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                LOG.info("BT service disconnected!");
                if (recurringIntent != null) {
                    getAlarmManager().cancel(recurringIntent);
                    recurringIntent = null;
                }
                getApplicationContext().unregisterReceiver(alarmReceiver);
                dataMediator.removeOtherEnd(btService);
                btServiceConnection = null;
            }

            private AlarmManager getAlarmManager() {
                return (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            }
        };
        this.bindService(btIntent, btServiceConnection, 0);
    }

    public DataMediator getDataMediator() {
        return dataMediator;
    }

    private class BtStateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        LOG.warn("Bluetooth was disabled externally, trying to re-enable");
                        BluetoothAdapter.getDefaultAdapter().enable();
                        break;
                    case BluetoothAdapter.STATE_ON:
                        LOG.debug("Bluetooth enabled, starting service");
                        startBTServiceIfNotStarted();
                        break;
                }
            }
        }
    }
}
