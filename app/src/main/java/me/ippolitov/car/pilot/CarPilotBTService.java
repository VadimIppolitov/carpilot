package me.ippolitov.car.pilot;

import android.app.IntentService;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import me.ippolitov.car.lib.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.UUID;

public class CarPilotBTService extends IntentService implements DataMediatorSink {

    private Logger LOG = LoggerFactory.getLogger(CarPilotBTService.class);

    private static final UUID SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private static final String HC_05 = "98:D3:31:B3:D1:C4";
    private static final String W_DEXP = "5C:93:A2:A9:62:43";
    private static final String remoteMac = HC_05;

    private static final byte[] PREAMBLE = new byte[4];

    private DataMediator dataMediator;
    private BluetoothSocket btSocket = null;
    private OutputStream output = null;

    public CarPilotBTService() {
        super("CarPilotBTService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        LOG.debug("Service started");
        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        DataInputStream input = null;
        try {
            //noinspection InfiniteLoopStatement
            while (true) {
                try {
                    if (btSocket == null || input == null || output == null) {
                        btSocket = getBluetoothSocket(btAdapter);
                        input = new DataInputStream(btSocket.getInputStream());
                        output = btSocket.getOutputStream();
                    }
                    ByteBuffer byteBuffer = readPacket(input);
                    dataMediator.sendViaUDP(Packet.builder().putMagic().putData(byteBuffer).build());
                } catch (Exception e) {
                    if ("Service discovery failed".equals(e.getMessage())) {
                        LOG.warn("Bluetooth device not responding, will try to reconnect");
                    } else {
                        LOG.warn("Exception in loop, will try to reconnect", e);
                    }
                    tryCloseBtSocket();
                    Utils.sleep(1000);
                    btSocket = null;
                }
            }
        } finally {
            LOG.trace("Loop finished");
            tryCloseBtSocket();
        }
    }

    private ByteBuffer readPacket(DataInputStream input) throws IOException {
        LOG.trace("Syncing on the preamble");
        int markersRemaining = PREAMBLE.length;
        while (markersRemaining > 0) {
            int b = input.readByte();
            if (b == 0) {
                markersRemaining--;
            } else {
                markersRemaining = PREAMBLE.length;
            }
        }
        LOG.trace("Reading packet size");
        int packetSize = input.readByte();
        LOG.debug("Reading " + packetSize + " data bytes...");
        byte[] buf = new byte[packetSize];
        input.readFully(buf);
        ByteBuffer wrappedBuf = ByteBuffer.wrap(buf);
        LOG.debug("Got: " + Utils.debugBufStr(wrappedBuf));
        return wrappedBuf;
    }

    private void tryCloseBtSocket() {
        if (btSocket != null) {
            try {
                btSocket.close();
            } catch (Exception ce) {
                LOG.error("Exception when closing socket", ce);
            }
        }
    }

    private BluetoothSocket getBluetoothSocket(BluetoothAdapter btAdapter) throws IOException {
        BluetoothDevice device = btAdapter.getRemoteDevice(remoteMac);
        BluetoothSocket btSocket = device.createRfcommSocketToServiceRecord(SPP_UUID);
        try {
            btAdapter.cancelDiscovery();
            LOG.trace("Connecting BT socket");
            btSocket.connect();
            return btSocket;
        } catch (IOException e) {
            tryCloseBtSocket();
            throw e;
        } catch (Exception e) {
            tryCloseBtSocket();
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public void setDataMediator(DataMediator dataMediator) {
        this.dataMediator = dataMediator;
    }

    @Override
    public boolean handlePacket(final Packet packet) {
        if (output == null) {
            LOG.warn("Packet discarded: no Bluetooth connection");
            return false;
        }
        try {
            ByteBuffer buffer = packet.byteBuffer();
            output.write(PREAMBLE);
            output.write(buffer.remaining());
            output.write(buffer.array(), buffer.position(), buffer.remaining());
            LOG.trace("Sent {} bytes via BT", buffer.remaining());
            return true;
        } catch (Exception e) {
            LOG.error("Exception when writing data", e);
            tryCloseBtSocket();
            return false;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new BTBinder();
    }

    public class BTBinder extends Binder {
        public CarPilotBTService getService() {
            return CarPilotBTService.this;
        }
    }
}
